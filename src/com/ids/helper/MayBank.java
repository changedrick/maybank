/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ids.constant.CommonSetting;
import com.ids.model.ReqInquiry;
import com.ids.model.ReqPayment;
import com.ids.model.ReqReversal;
import com.ids.model.ReqTransfer;
import com.ids.model.ResInquiry;
import com.ids.model.ResPayment;
import com.ids.model.ResReversal;
import com.ids.model.ResToken;
import com.ids.model.ResTransfer;
import com.ids.model.object.Msg;
import com.ids.model.object.RequestMsg;
import com.ids.model.object.ResponseMsg;
import com.ids.util.DefaultTrustManager;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import org.json.JSONObject;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class MayBank {
    
    public ResToken getAccessToken() throws Exception {
        ResToken resToken = new ResToken();
        
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);
        
        String url  = CommonSetting.MBBaseURL + CommonSetting.MBAuthPort + CommonSetting.MBAuthURL
                    + "?client_id="           + CommonSetting.clientID
                    + "&client_secret="       + CommonSetting.clientSecret
                    + "&grant_type="          + CommonSetting.grant_type;
        
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        OutputStream os = con.getOutputStream();
        os.close();

        StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        JSONObject myResponse = new JSONObject(response.toString());

        resToken.setAccess_token(myResponse.getString("access_token"));
        resToken.setToken_type(myResponse.getString("token_type"));
        resToken.setExpires_in(myResponse.getInt("expires_in"));
        
        con.disconnect();
        
        return resToken;
        
    }
    
    public ResInquiry Inquiry(ResToken resToken, ReqInquiry reqInquiry) throws Exception {
        ResInquiry resInquiry;
        
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);
        
        String url = CommonSetting.MBBaseURL + CommonSetting.MBTransPort + CommonSetting.MBInquiryURL;
        System.out.println("\nEndpoint :"   + url + "\n");
        
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", resToken.getToken_type() + resToken.getAccess_token());
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        OutputStream os = con.getOutputStream();
        os.write(gson.toJson(reqInquiry).getBytes("UTF-8"));
        os.close();

        StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        
        JSONObject jsonObject = new JSONObject(response.toString());
        JSONObject jsonObject1 = jsonObject.getJSONObject("ResponseMsg");
        JSONObject jsonObject2 = jsonObject.getJSONObject("RequestMsg").getJSONObject("Msg");

        Msg msg = new Msg();
        msg.setMTI(jsonObject2.getString("MTI"));
        msg.setProcessingCode(jsonObject2.getString("processingCode"));
        msg.setTrxAmount(jsonObject2.getString("trxAmount"));
        msg.setTrxDateTime(jsonObject2.getString("trxDateTime")); 
        msg.setTerminalTraceNo(jsonObject2.getString("terminalTraceNo")); 
        msg.setLocalTimeTrx(jsonObject2.getString("localTimeTrx"));
        msg.setLocalDateTrx(jsonObject2.getString("localDateTrx"));
        msg.setPosEntryCode(jsonObject2.getString("posEntryCode"));
        msg.setAcqIdentificationCode(jsonObject2.getString("acqIdentificationCode"));
        msg.setRRN(jsonObject2.getString("RRN"));
        msg.setHostRC(jsonObject2.getString("hostRC")); 
        msg.setCaTerminalIdentification(jsonObject2.getString("caTerminalIdentification"));
        //msg.setCaIdentification(jsonObject2.getString("caIdentification"));
        //msg.setCaNameLocation(jsonObject2.getString("caNameLocation"));
        msg.setAddData(jsonObject2.getString("addData"));
        msg.setCurrencyCode(jsonObject2.getString("currencyCode"));
        
        if(jsonObject1.getString("responseCode").equals("00")){
            //msg.setInfoData(jsonObject2.getString("infoData"));
        }
        
        RequestMsg  requestMsg = new RequestMsg(msg);
        
        ResponseMsg responseMsg = new ResponseMsg();
        
        responseMsg.setResponseCode(jsonObject1.getString("responseCode"));
        responseMsg.setResponseDesc(jsonObject1.getString("responseDesc"));
        
        resInquiry = new ResInquiry(responseMsg, requestMsg);
        
        con.disconnect();
        
        return resInquiry;
        
    }
    
    public ResPayment Payment(ResToken resToken, ReqPayment reqPayment) throws Exception {
        ResPayment resPayment;
        
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);
        
        String url  = CommonSetting.MBBaseURL + CommonSetting.MBTransPort + CommonSetting.MBPaymentURL;
        System.out.println("\nEndpoint :"   + url + "\n");
        
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", resToken.getToken_type() + resToken.getAccess_token());
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        OutputStream os = con.getOutputStream();
        os.write(gson.toJson(reqPayment).getBytes("UTF-8"));
        os.close();

        StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        
        JSONObject jsonObject = new JSONObject(response.toString());
        JSONObject jsonObject1 = jsonObject.getJSONObject("ResponseMsg");
        JSONObject jsonObject2 = jsonObject.getJSONObject("RequestMsg").getJSONObject("Msg");

        Msg msg = new Msg();
        msg.setMTI(jsonObject2.getString("MTI"));
        msg.setProcessingCode(jsonObject2.getString("processingCode"));
        msg.setTrxAmount(jsonObject2.getString("trxAmount"));
        msg.setTrxDateTime(jsonObject2.getString("trxDateTime")); 
        msg.setTerminalTraceNo(jsonObject2.getString("terminalTraceNo")); 
        msg.setLocalTimeTrx(jsonObject2.getString("localTimeTrx"));
        msg.setLocalDateTrx(jsonObject2.getString("localDateTrx"));
        msg.setPosEntryCode(jsonObject2.getString("posEntryCode"));
        msg.setAcqIdentificationCode(jsonObject2.getString("acqIdentificationCode"));
        msg.setRRN(jsonObject2.getString("RRN"));
        msg.setHostRC(jsonObject2.getString("hostRC")); 
        msg.setCaTerminalIdentification(jsonObject2.getString("caTerminalIdentification"));
        //msg.setCaIdentification(jsonObject2.getString("caIdentification"));
        //msg.setCaNameLocation(jsonObject2.getString("caNameLocation"));
        msg.setAddData(jsonObject2.getString("addData"));
        msg.setCurrencyCode(jsonObject2.getString("currencyCode"));
        
        if(jsonObject1.getString("responseCode").equals("00")){
            //msg.setInfoData(jsonObject2.getString("infoData"));
        }
        
        RequestMsg  requestMsg = new RequestMsg(msg);
        
        ResponseMsg responseMsg = new ResponseMsg();
        
        responseMsg.setResponseCode(jsonObject1.getString("responseCode"));
        responseMsg.setResponseDesc(jsonObject1.getString("responseDesc"));
        
        resPayment = new ResPayment(responseMsg, requestMsg);
        
        con.disconnect();
        
        return resPayment;
        
    }
    
    public ResTransfer Transfer(String url, ResToken resToken, ReqTransfer reqTransfer) throws Exception {
        ResTransfer resTransfer;
        
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);
        
        System.out.println("\nEndpoint :"   + url + "\n");
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", resToken.getToken_type() + resToken.getAccess_token());
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        OutputStream os = con.getOutputStream();
        os.write(gson.toJson(reqTransfer).getBytes("UTF-8"));
        os.close();

        StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        
        JSONObject jsonObject = new JSONObject(response.toString());
        JSONObject jsonObject1 = jsonObject.getJSONObject("ResponseMsg");
        JSONObject jsonObject2 = jsonObject.getJSONObject("RequestMsg").getJSONObject("Msg");

        Msg msg = new Msg();
        msg.setMTI(jsonObject2.getString("MTI"));
        msg.setProcessingCode(jsonObject2.getString("processingCode"));
        msg.setTrxAmount(jsonObject2.getString("trxAmount"));
        msg.setTrxDateTime(jsonObject2.getString("trxDateTime")); 
        msg.setTerminalTraceNo(jsonObject2.getString("terminalTraceNo")); 
        msg.setLocalTimeTrx(jsonObject2.getString("localTimeTrx"));
        msg.setLocalDateTrx(jsonObject2.getString("localDateTrx"));
        msg.setPosEntryCode(jsonObject2.getString("posEntryCode"));
        msg.setAcqIdentificationCode(jsonObject2.getString("acqIdentificationCode"));
        msg.setRRN(jsonObject2.getString("RRN"));
        msg.setHostRC(jsonObject2.getString("hostRC")); 
        msg.setCaTerminalIdentification(jsonObject2.getString("caTerminalIdentification"));
        msg.setCaIdentification(jsonObject2.getString("caIdentification"));
        msg.setCaNameLocation(jsonObject2.getString("caNameLocation"));
        msg.setAddData(jsonObject2.getString("addData"));
        msg.setCurrencyCode(jsonObject2.getString("currencyCode"));
        
        if(jsonObject1.getString("responseCode").equals("00")){
            //msg.setInfoData(jsonObject2.getString("infoData"));
        }
        
        RequestMsg  requestMsg = new RequestMsg(msg);
        
        ResponseMsg responseMsg = new ResponseMsg();
        
        responseMsg.setResponseCode(jsonObject1.getString("responseCode"));
        responseMsg.setResponseDesc(jsonObject1.getString("responseDesc"));
        
        resTransfer = new ResTransfer(responseMsg, requestMsg);
        
        con.disconnect();
        
        return resTransfer;
    }
    
    public ResReversal Reversal(ResToken resToken, ReqReversal reqReversal) throws Exception {
        ResReversal resReversal;
        
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);
        
        String url  = CommonSetting.MBBaseURL + CommonSetting.MBTransPort + CommonSetting.MBReverseURL;
        System.out.println("\nEndpoint :"   + url + "\n");
        
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", resToken.getToken_type() + resToken.getAccess_token());
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        OutputStream os = con.getOutputStream();
        os.write(gson.toJson(reqReversal).getBytes("UTF-8"));
        os.close();

        StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        
        JSONObject jsonObject = new JSONObject(response.toString());
        JSONObject jsonObject1 = jsonObject.getJSONObject("ResponseMsg");
        JSONObject jsonObject2 = jsonObject.getJSONObject("RequestMsg").getJSONObject("Msg");

        Msg msg = new Msg();
        msg.setMTI(jsonObject2.getString("MTI"));
        msg.setProcessingCode(jsonObject2.getString("processingCode"));
        msg.setTrxAmount(jsonObject2.getString("trxAmount"));
        msg.setTrxDateTime(jsonObject2.getString("trxDateTime")); 
        msg.setTerminalTraceNo(jsonObject2.getString("terminalTraceNo")); 
        msg.setLocalTimeTrx(jsonObject2.getString("localTimeTrx"));
        msg.setLocalDateTrx(jsonObject2.getString("localDateTrx"));
        msg.setPosEntryCode(jsonObject2.getString("posEntryCode"));
        msg.setAcqIdentificationCode(jsonObject2.getString("acqIdentificationCode"));
        msg.setRRN(jsonObject2.getString("RRN"));
        msg.setHostRC(jsonObject2.getString("hostRC")); 
        msg.setCaTerminalIdentification(jsonObject2.getString("caTerminalIdentification"));
        //msg.setCaIdentification(jsonObject2.getString("caIdentification"));
        //msg.setCaNameLocation(jsonObject2.getString("caNameLocation"));
        msg.setAddData(jsonObject2.getString("addData"));
        msg.setCurrencyCode(jsonObject2.getString("currencyCode"));
        
        if(jsonObject1.getString("responseCode").equals("00")){
            //msg.setInfoData(jsonObject2.getString("infoData"));
        }
        
        RequestMsg  requestMsg = new RequestMsg(msg);
        
        ResponseMsg responseMsg = new ResponseMsg();
        
        responseMsg.setResponseCode(jsonObject1.getString("responseCode"));
        responseMsg.setResponseDesc(jsonObject1.getString("responseDesc"));
        
        resReversal = new ResReversal(responseMsg, requestMsg);
        
        con.disconnect();
        
        return resReversal;
        
    }
}
