/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.model.object;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class Msg {
    private String MTI;
    private String processingCode;
    private String trxAmount;
    private String trxDateTime;
    private String terminalTraceNo;
    private String localTimeTrx;
    private String localDateTrx;
    private String posEntryCode;
    private String acqIdentificationCode;
    private String RRN;
    private String hostRC;
    private String caTerminalIdentification;
    private String caIdentification;
    private String caNameLocation;
    private String addData;
    private String currencyCode;
    private String infoData;
    private String oriDataElement;
    private String payee;
    private String fromAccount;
    private String toAccount;

    public Msg(){
        
    }

    public String getMTI() {
        return MTI;
    }

    public void setMTI(String MTI) {
        this.MTI = MTI;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getTrxAmount() {
        return trxAmount;
    }

    public void setTrxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
    }

    public String getTrxDateTime() {
        return trxDateTime;
    }

    public void setTrxDateTime(String trxDateTime) {
        this.trxDateTime = trxDateTime;
    }

    public String getTerminalTraceNo() {
        return terminalTraceNo;
    }

    public void setTerminalTraceNo(String terminalTraceNo) {
        this.terminalTraceNo = terminalTraceNo;
    }

    public String getLocalTimeTrx() {
        return localTimeTrx;
    }

    public void setLocalTimeTrx(String localTimeTrx) {
        this.localTimeTrx = localTimeTrx;
    }

    public String getLocalDateTrx() {
        return localDateTrx;
    }

    public void setLocalDateTrx(String localDateTrx) {
        this.localDateTrx = localDateTrx;
    }

    public String getPosEntryCode() {
        return posEntryCode;
    }

    public void setPosEntryCode(String posEntryCode) {
        this.posEntryCode = posEntryCode;
    }

    public String getAcqIdentificationCode() {
        return acqIdentificationCode;
    }

    public void setAcqIdentificationCode(String acqIdentificationCode) {
        this.acqIdentificationCode = acqIdentificationCode;
    }

    public String getRRN() {
        return RRN;
    }

    public void setRRN(String RRN) {
        this.RRN = RRN;
    }

    public String getHostRC() {
        return hostRC;
    }

    public void setHostRC(String hostRC) {
        this.hostRC = hostRC;
    }

    public String getCaTerminalIdentification() {
        return caTerminalIdentification;
    }

    public void setCaTerminalIdentification(String caTerminalIdentification) {
        this.caTerminalIdentification = caTerminalIdentification;
    }

    public String getCaIdentification() {
        return caIdentification;
    }

    public void setCaIdentification(String caIdentification) {
        this.caIdentification = caIdentification;
    }

    public String getCaNameLocation() {
        return caNameLocation;
    }

    public void setCaNameLocation(String caNameLocation) {
        this.caNameLocation = caNameLocation;
    }

    public String getAddData() {
        return addData;
    }

    public void setAddData(String addData) {
        this.addData = addData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getInfoData() {
        return infoData;
    }

    public void setInfoData(String infoData) {
        this.infoData = infoData;
    }

    public String getOriDataElement() {
        return oriDataElement;
    }

    public void setOriDataElement(String oriDataElement) {
        this.oriDataElement = oriDataElement;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }
    
    
       
}
