/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.model.object;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class RequestMsg {
    private Header Header;
    private Msg Msg;

    public RequestMsg(Header Header, Msg Msg) {
        this.Header = Header;
        this.Msg = Msg;
    }

    public RequestMsg(Msg Msg) {
        this.Msg = Msg;
    }

    public Header getHeader() {
        return Header;
    }

    public void setHeader(Header Header) {
        this.Header = Header;
    }

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg Msg) {
        this.Msg = Msg;
    }

    
    
}
