/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.model.object;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class Header {
    private String messageID;
    private String channelID;
    private String mitraID;
    private String reference;
    private String trandatetime;

    public Header() {
    }

    public Header(String messageID, String channelID, String mitraID, String reference, String trandatetime) {
        this.messageID = messageID;
        this.channelID = channelID;
        this.mitraID = mitraID;
        this.reference = reference;
        this.trandatetime = trandatetime;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public String getMitraID() {
        return mitraID;
    }

    public void setMitraID(String mitraID) {
        this.mitraID = mitraID;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTrandatetime() {
        return trandatetime;
    }

    public void setTrandatetime(String trandatetime) {
        this.trandatetime = trandatetime;
    }
    
    
}
