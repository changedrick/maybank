/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.model;

import com.ids.model.object.RequestMsg;
import com.ids.model.object.ResponseMsg;

/**
 *
 * @author boids020
 */
public class ResTransfer {
    private ResponseMsg responseMsg;
    private RequestMsg requestMsg;

    public ResTransfer(ResponseMsg responseMsg, RequestMsg requestMsg) {
        this.responseMsg = responseMsg;
        this.requestMsg = requestMsg;
    }
    
    public ResponseMsg getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(ResponseMsg responseMsg) {
        this.responseMsg = responseMsg;
    }

    public RequestMsg getRequestMsg() {
        return requestMsg;
    }

    public void setRequestMsg(RequestMsg requestMsg) {
        this.requestMsg = requestMsg;
    }
}
