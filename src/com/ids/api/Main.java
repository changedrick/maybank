/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.api;

import com.ids.model.ReqInquiry;
import com.ids.model.ReqPayment;
import com.ids.model.ResInquiry;
import com.ids.model.ResPayment;
import com.ids.model.ResToken;
import com.ids.model.object.Header;
import com.ids.model.object.Msg;
import com.ids.model.object.RequestMsg;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ids.constant.CommonSetting;
import com.ids.helper.MayBank;
import com.ids.model.ReqReversal;
import com.ids.model.ReqTransfer;
import com.ids.model.ResReversal;
import com.ids.model.ResTransfer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class Main {

    private static final String channelID = CommonSetting.channelID;
    private static final String mitraID = CommonSetting.mitraID;
    private static final String processingCodeSavingAccount = "10"; // 10 : from Saving Account
    private static final String processingCodeCheckingAccount = "20"; // 20 : from Checking Account
    private static final String AddDataOnline = "0002  00"; // 0002  00
    private static final String AddDataOffline = "6005  00"; // 6005  00
    private static final String caIdentification = "081113010338"; // 302000224140
    private static final String caNameLocation = "081113010338"; // CUSTOMER 302000224140
    private static final String Payee = "085966444100"; // 085966444100
    
    
    
    public static void main(String[] args) throws Exception {
        ReqInquiry  reqInquiry       = new ReqInquiry();
        ReqPayment  reqPayment       = new ReqPayment();
        ReqReversal reqReversal      = new ReqReversal();
        ReqTransfer reqTransferInquiry     = new ReqTransfer();
        ReqTransfer reqTransferSettle      = new ReqTransfer();
        
        ResToken resToken;
        ResInquiry billInquiry;
        ResPayment billPaymentAndPurchase;
        ResReversal reversalAndAdvice;
        ResTransfer transferInquiryInfo;
        ResTransfer transferSettle;
                       
        resToken = getAccessToken();
        // i
        billInquiry = billInquiry(resToken, reqInquiry);
        // ii
        billPaymentAndPurchase = billPaymentAndPurchase(resToken, reqPayment);
        // iii
        transferInquiryInfo = transferInquiryInfo(resToken, reqTransferInquiry);
        // iv
        transferSettle = transferSettle(resToken, reqTransferSettle);
        // v
        reversalAndAdvice = reversalAndAdvice(resToken, reqReversal);
        
        /*printResponse(resToken, billInquiry, 
                        transferInquiryInfo, transferSettle,
                        billPaymentAndPurchase, reversalAndAdvice, 
                        reqInquiry, reqTransferInquiry, reqTransferSettle, 
                        reqPayment, reqReversal);*/
        
    }
    
    public static ResToken getAccessToken() throws Exception{
        MayBank     mbHelper    = new MayBank();
        ResToken resToken;
        
        resToken = mbHelper.getAccessToken();
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        System.out.println("\nRESPONSE TOKEN \n" + gson.toJson(resToken));
        
        return resToken;
    }
    
    // i. Bill Inquiry
    public static ResInquiry billInquiry(ResToken resToken, ReqInquiry reqInquiry) throws Exception{
        Header     headers          = new Header();
        Msg        msg              = new Msg();
        MayBank    mbHelper         = new MayBank();
        RequestMsg requestMsgs;
        ResInquiry resInquiry;
        
        UUID uuid = UUID.randomUUID();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");  
        Date date = new Date(System.currentTimeMillis());
        String setTrandatetime = formatter.format(date);

        headers.setMessageID(uuid.toString());
        headers.setChannelID(channelID);
        headers.setMitraID(mitraID);
        headers.setReference(channelID + "/" + mitraID);
        headers.setTrandatetime(setTrandatetime);
        
        msg.setMTI("0200"); 
        msg.setProcessingCode("38"+processingCodeSavingAccount+"00"); 
        msg.setTrxAmount("0000000000000000");
        msg.setTrxDateTime("0305100502");
        msg.setTerminalTraceNo("947870");
        msg.setLocalTimeTrx("053755");
        msg.setLocalDateTrx("1219");
        msg.setPosEntryCode("018");
        msg.setAcqIdentificationCode(mitraID); 
        msg.setRRN("000000218074"); 
        msg.setCaTerminalIdentification(mitraID);
        msg.setCaIdentification(caIdentification);
        msg.setCaNameLocation(caNameLocation);
        msg.setAddData(AddDataOnline);
        msg.setCurrencyCode("360");
        msg.setPayee(Payee);
        msg.setFromAccount("0162003500382");
        
        requestMsgs = new RequestMsg(headers, msg);
        
        reqInquiry.setRequestMsg(requestMsgs);
        resInquiry = mbHelper.Inquiry(resToken, reqInquiry);
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        System.out.println("\nREQUEST i. Bill Inquiry:\n"   + gson.toJson(reqInquiry));
        System.out.println("\nRESPONSE i. Bill Inquiry:\n"   + gson.toJson(resInquiry));
        
        return resInquiry;
    }
    
    // ii. Bill Payment and Purchase
    public static ResPayment billPaymentAndPurchase(ResToken resToken, ReqPayment reqPayment) throws Exception{
        Header     headers          = new Header();
        Msg        msg              = new Msg();
        MayBank    mbHelper         = new MayBank();
        RequestMsg requestMsgs;
        ResPayment resPayment;
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        UUID uuid = UUID.randomUUID();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");  
        Date date = new Date(System.currentTimeMillis());
        String setTrandatetime = formatter.format(date);

        headers.setMessageID(uuid.toString());
        headers.setChannelID(channelID);
        headers.setMitraID(mitraID);
        headers.setReference(channelID + "/" + mitraID);
        headers.setTrandatetime(setTrandatetime);
        
        msg.setMTI("0200"); 
        msg.setProcessingCode("18"+processingCodeCheckingAccount+"00"); 
        msg.setTrxAmount("000000437500");
        msg.setTrxDateTime("0305100502");
        msg.setTerminalTraceNo("947870");
        msg.setLocalTimeTrx("053755");
        msg.setLocalDateTrx("1219");
        msg.setPosEntryCode("018");
        msg.setAcqIdentificationCode(mitraID); 
        msg.setRRN("000000218074"); 
        msg.setCaTerminalIdentification(mitraID);
        msg.setCaIdentification(caIdentification);
        msg.setCaNameLocation(caNameLocation);
        msg.setAddData(AddDataOffline);
        msg.setCurrencyCode("360");
        msg.setPayee("085966444100");
        msg.setFromAccount("0162003500382");
        
        requestMsgs = new RequestMsg(headers, msg);        
        reqPayment.setRequestMsg(requestMsgs);        
        resPayment = mbHelper.Payment(resToken, reqPayment);        
        
        System.out.println("\nREQUEST ii. Bill Payment and Purchase:\n"   + gson.toJson(reqPayment));
        System.out.println("\nRESPONSE ii. Bill Payment and Purchase:\n"   + gson.toJson(resPayment));
            
        return resPayment;
    }
    
    // iii. Transfer Inquiry Info
    public static ResTransfer transferInquiryInfo(ResToken resToken, ReqTransfer reqTransferInquiry) {
        Header     headers          = new Header();
        Msg        msg              = new Msg();
        MayBank    mbHelper         = new MayBank();
        RequestMsg requestMsgs;
        ResTransfer resTransferInquiry = null;
        
        UUID uuid = UUID.randomUUID();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");  
        Date date = new Date(System.currentTimeMillis());
        String setTrandatetime = formatter.format(date);

        headers.setMessageID(uuid.toString());
        headers.setChannelID(channelID);
        headers.setMitraID(mitraID);
        headers.setReference(channelID + "/" + mitraID);
        headers.setTrandatetime(setTrandatetime);
        
        msg.setMTI("0200"); 
        msg.setProcessingCode("37"+processingCodeSavingAccount+"00"); 
        msg.setTrxAmount("0000000000437500");
        msg.setTrxDateTime("0305100502");
        msg.setTerminalTraceNo("947870");
        msg.setLocalTimeTrx("053755");
        msg.setLocalDateTrx("1219");
        msg.setPosEntryCode("018");
        msg.setAcqIdentificationCode(mitraID); 
        msg.setRRN("000000218074"); 
        msg.setCaTerminalIdentification(mitraID);
        msg.setCaIdentification(caIdentification);
        msg.setCaNameLocation(caNameLocation);
        msg.setAddData(AddDataOffline);
        msg.setCurrencyCode("360");
        msg.setPayee(Payee);
        msg.setFromAccount("0162003500382");
        //msg.setToAccount("0162003500382");
        
        requestMsgs = new RequestMsg(headers, msg);
        
        reqTransferInquiry.setRequestMsg(requestMsgs);
        try {
            String url  = CommonSetting.MBBaseURL + CommonSetting.MBTransPort + CommonSetting.MBInquiryURL;
            resTransferInquiry = mbHelper.Transfer(url, resToken, reqTransferInquiry);
            
            Gson gson;
            GsonBuilder builder = new GsonBuilder();
            builder.setPrettyPrinting();
            gson = builder.create();

            System.out.println("\nREQUEST iii. Transfer Inquiry Info:\n"   + gson.toJson(reqTransferInquiry));
            System.out.println("\nRESPONSE iii. Transfer Inquiry Info:\n"   + gson.toJson(resTransferInquiry));

        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        return resTransferInquiry;
    }
    
    // iv. Transfer Settle
    public static ResTransfer transferSettle(ResToken resToken, ReqTransfer reqTransferSettle) {
        Header     headers          = new Header();
        Msg        msg              = new Msg();
        MayBank    mbHelper         = new MayBank();
        RequestMsg requestMsgs;
        ResTransfer resTransferSettle = null;
        
        UUID uuid = UUID.randomUUID();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");  
        Date date = new Date(System.currentTimeMillis());
        String setTrandatetime = formatter.format(date);

        headers.setMessageID(uuid.toString());
        headers.setChannelID(channelID);
        headers.setMitraID(mitraID);
        headers.setReference(channelID + "/" + mitraID);
        headers.setTrandatetime(setTrandatetime);
        
        msg.setMTI("0200"); 
        msg.setProcessingCode("47"+processingCodeCheckingAccount+"00"); 
        msg.setTrxAmount("0000000000437500");
        msg.setTrxDateTime("0305100502");
        msg.setTerminalTraceNo("947870");
        msg.setLocalTimeTrx("053755");
        msg.setLocalDateTrx("1219");
        msg.setPosEntryCode("018");
        msg.setAcqIdentificationCode(mitraID); 
        msg.setRRN("000000218074"); 
        msg.setCaTerminalIdentification(mitraID);
        msg.setCaIdentification(caIdentification);
        msg.setCaNameLocation(caNameLocation);
        msg.setAddData(AddDataOnline);
        msg.setCurrencyCode("360");
        //msg.setPayee(Payee);
        msg.setFromAccount("0162003500382");
        msg.setToAccount("0162003500382");
        
        requestMsgs = new RequestMsg(headers, msg);
        
        reqTransferSettle.setRequestMsg(requestMsgs);
        try {
            String url  = CommonSetting.MBBaseURL + CommonSetting.MBTransPort + CommonSetting.MBPaymentURL;
            resTransferSettle = mbHelper.Transfer(url, resToken, reqTransferSettle);
            
            Gson gson;
            GsonBuilder builder = new GsonBuilder();
            builder.setPrettyPrinting();
            gson = builder.create();

            System.out.println("\nREQUEST iv. Transfer Settle:\n"   + gson.toJson(reqTransferSettle));
            System.out.println("\nRESPONSE iv. Transfer Settle:\n"   + gson.toJson(resTransferSettle));
            
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        return resTransferSettle;
    }
    
    // v. Reversal and Advice
    public static ResReversal reversalAndAdvice(ResToken resToken, ReqReversal reqReversal) throws Exception{
        Header      headers          = new Header();
        Msg         msg              = new Msg();
        MayBank     mbHelper         = new MayBank();
        RequestMsg  requestMsgs;
        ResReversal resReversal;
        
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        UUID uuid = UUID.randomUUID();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");  
        Date date = new Date(System.currentTimeMillis());
        String setTrandatetime = formatter.format(date);

        headers.setMessageID(uuid.toString());
        headers.setChannelID(channelID);
        headers.setMitraID(mitraID);
        headers.setReference(channelID + "/" + mitraID);
        headers.setTrandatetime(setTrandatetime);
        
        msg.setMTI("0400"); 
        msg.setProcessingCode("18"+processingCodeCheckingAccount+"00"); 
        msg.setTrxAmount("000000437500");
        msg.setTrxDateTime("0305100502");
        msg.setTerminalTraceNo("947870");
        msg.setLocalTimeTrx("053755");
        msg.setLocalDateTrx("1219");
        msg.setPosEntryCode("018");
        msg.setAcqIdentificationCode(mitraID); 
        msg.setRRN("000000218074"); 
        msg.setCaTerminalIdentification(mitraID);
        msg.setCaIdentification(caIdentification);
        msg.setCaNameLocation(caNameLocation);
        msg.setAddData(AddDataOffline);
        msg.setCurrencyCode("360");
        /*
        OriDataElement
        MTI = 0400
        DE011 = terminalTraceNo (947870)
        DE007 = trxDateTime (0305100502)
        DE032 = acqIdentificationCode (mitraID)
        Zero 11 char = 00000000000
        */
        msg.setOriDataElement("04009478700305100502" + mitraID + "00000000000");
        msg.setPayee(Payee);
        msg.setFromAccount("0162003500382");
        msg.setToAccount("0162003500381");
        
        requestMsgs = new RequestMsg(headers, msg);
        
        reqReversal.setRequestMsg(requestMsgs);        
        resReversal = mbHelper.Reversal(resToken, reqReversal);        

        System.out.println("\nREQUEST v. Reversal and Advice:\n"   + gson.toJson(reqReversal));
        System.out.println("\nRESPONSE v. Reversal and Advice:\n"   + gson.toJson(resReversal));
        
        return resReversal;
    }
    
    
    
    /*public static void printResponse(ResToken resToken, ResInquiry resInquiry, 
                                    ResTransfer transferInquiryInfo, 
                                    ResTransfer transferSettle,
                                    ResPayment resPayment, ResReversal resReversal,
                                    ReqInquiry reqInquiry, 
                                    ReqTransfer reqTransferInquiry, ReqTransfer reqTransferSettle, 
                                    ReqPayment reqPayment, ReqReversal reqReversal){
        Gson gson;
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
        
        System.out.println("\nRESPONSE TOKEN \n" + gson.toJson(resToken));
        System.out.println("\nREQUEST i. Bill Inquiry:\n"   + gson.toJson(reqInquiry));
        System.out.println("\nRESPONSE i. Bill Inquiry:\n"   + gson.toJson(resInquiry));
        System.out.println("\nREQUEST ii. Bill Payment and Purchase:\n"   + gson.toJson(reqPayment));
        System.out.println("\nRESPONSE ii. Bill Payment and Purchase:\n"   + gson.toJson(resPayment));
        
        System.out.println("\nREQUEST iii. Transfer Inquiry Info:\n"   + gson.toJson(reqTransferInquiry));
        System.out.println("\nRESPONSE iii. Transfer Inquiry Info:\n"   + gson.toJson(transferInquiryInfo));
                
        System.out.println("\nREQUEST iv. Transfer Settle:\n"   + gson.toJson(reqTransferSettle));
        System.out.println("\nRESPONSE iv. Transfer Settle:\n"   + gson.toJson(transferSettle));
        
        System.out.println("\nREQUEST v. Reversal and Advice:\n"   + gson.toJson(reqReversal));
        System.out.println("\nRESPONSE v. Reversal and Advice:\n"   + gson.toJson(resReversal));
    }*/
}
