/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ids.constant;

/**
 *
 * @author Edrick <your.name at your.org>
 */
public class CommonSetting {
    public static final String clientID = "2f1c8c21c838b8062e98";
    public static final String clientSecret = "xIM_7QtptZ-v5Yw4H6oqjgwmI5U";
    public static final String grant_type = "client_credentials";
    
    //====================== Mitra ID =========================================
    public static final String mitraID = "62002100241";
    public static final String channelID = "MBS";
    
    //======================MAY BANK============================================
    public static final String MBBaseURL = "https://sandbox.maybank.co.id:";
    public static final String MBAuthPort = "8092";
    public static final String MBAuthURL = "/test_domain/oauth/token";
    public static final String MBTransPort = "8082";
    public static final String MBInquiryURL = "/mitra-bisnis/v1/inquiry";
    public static final String MBPaymentURL = "/mitra-bisnis/v1/payment";
    public static final String MBReverseURL = "/mitra-bisnis/v1/reverse";
}
